''' @file    fp_motor_driver.py
    @brief   Interfaces motor hardware to perform various functions on the motors
    @details The motor driver can turn on and set the duty cycle of the motors
    @author  Ishan Jandaur
    @author  Soren Barclay
    @date    December 9, 2021
'''

import pyb
import utime

class Motor:
    ''' 
    @brief Class for motor hardware
    @details Objects of this class can be used to apply PWM to a given
    DC motor.
    '''

    def __init__ (self, pin_A, pin_B, channel_A, channel_B):
        ''' 
        @brief Initializes and returns a motor object associated with the DRV8847.
        @details Objects of this class should not be instantiated
        directly. Instead, create a motor object and use
        that to create Motor objects using the method
        motor_driver.motor()
        @param pin_A terminal pin 1
        @param pin_B terminal pin 2
        @param channel_A timer pin 1
        @param channel_B timer pin 2
        
        '''
        tim3 = pyb.Timer(3, freq = 20000)
        pin1 = pyb.Pin(pin_A, pyb.Pin.OUT_PP)
        pin2 = pyb.Pin(pin_B, pyb.Pin.OUT_PP)
        self.tchA = tim3.channel(channel_A, pyb.Timer.PWM, pin=pin1)
        self.tchB = tim3.channel(channel_B, pyb.Timer.PWM, pin=pin2)

    
    def set_duty (self, duty):
        ''' 
        @brief Set the PWM duty cycle for the motor channel.
        @details This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed number holding the duty
        cycle of the PWM signal sent to the motor
        '''
        # if duty > 100: 
        #     duty = 100
        # if duty < -100:
        #     duty = -100
        if duty >= 0: #forward
            self.tchA.pulse_width_percent(duty)
            self.tchB.pulse_width_percent(0)
        else: #backward
            duty = abs(duty)
            self.tchA.pulse_width_percent(0)
            self.tchB.pulse_width_percent(duty)
            
    
