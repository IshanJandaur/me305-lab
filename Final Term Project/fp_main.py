''' @file       fp_main.py
    @brief      Runs tasks
    @details    Instantiates objects of driver classes and task classes. Tasks interface with drivers in order to collect data
                and control different components of the system. Tasks are ran in a for loop. 
    @author     Ishan Jandaur
    @author     Soren Barclay
    @date       December 9, 2021
'''

import pyb
import shares
import task_user
import task_motor
import motor_driver
import panel_driver
import task_panel
import task_IMU
import IMU_driver
import task_controller
import struct


def main():
    ''' @brief      The main program
        @details    The function instantiates shared variables that are read and written to by other files, 
                    Task objects as well as driver objects with pin locations are instantiated for each motor. 
                    The tasks are then run 10 ms each in a loop until there is a keyboard interrupt
    '''

    x = shares.Share(0)
    thetay = shares.Share(0)
    dx = shares.Share(0)
    dthetay = shares.Share(0)
    
    y = shares.Share(0)
    thetax = shares.Share(0)
    dy = shares.Share(0)
    dthetax = shares.Share(0)

    duty_cycle1 = shares.Share(0)
    duty_cycle2 = shares.Share(0)
    
    controller_state = shares.Share(0)
    

    
    
    
    ## Panel driver
    panel_drv = panel_driver.TouchPanel(pyb.Pin.cpu.A7, pyb.Pin.cpu.A1, pyb.Pin.cpu.A6, pyb.Pin.cpu.A0, 176, 100)
    
    ## IMU driver
    IMU_drv = IMU_driver.BNO055()
 
    ## Motor Driver objects 
    motor_1 = motor_driver.Motor(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2)
    motor_2 = motor_driver.Motor(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4)
    
    #######################################################################################################
    
    ## Task controller 
    tsk_controller = task_controller.Task_Controller(x, thetay, dx, dthetay, y, thetax, dy, dthetax, duty_cycle1, duty_cycle2, controller_state)
    
    ## Task panel
    tsk_panel = task_panel.Task_panel(panel_drv, x, dx, y, dy)
    
    ## Task IMU
    tsk_IMU = task_IMU.Task_IMU(IMU_drv, thetax, dthetax, thetay, dthetay)
      
    ## Task motor objects
    tsk_motor1 = task_motor.Task_Motor(motor_1, duty_cycle1)
    tsk_motor2 = task_motor.Task_Motor(motor_2, duty_cycle2)
    
    ## A user interface task object
    tsk_user = task_user.Task_User(controller_state)
    
    #######################################################################################################
   
    ## A list of tasks to run
    task_list = [tsk_controller, tsk_panel, tsk_IMU, tsk_motor1, tsk_motor2, tsk_user]
    # task_list = [tsk_IMU]
    
    while(True):
        try:
            for task in task_list:
                task.run()
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')
    
if __name__ == '__main__':
    main()

