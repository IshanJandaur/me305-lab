''' @file       fp_panel_driver.py
    @brief      Touch panel driver communicates directly with the hardware 
    @details    the driver scans the x, y and, z circuits and calculates the position based off predetermined constants
    @author     Ishan Jandaur
    @author     Soren Barclay
    @date       December 9, 2021
'''
import pyb
import utime
from ulab import numpy as np


class TouchPanel:
    '''@brief Touch panel class
       @details Interacts with panel driver to communicate the location of the ball 
    '''
    def __init__(self, x_p, x_m, y_p, y_m, length, width):
        '''@brief constructor of Touch Panel class
           @details Constructor that instantiates pins to set up the touch panel
           @param x_p x positive terminal
           @param x_m x negative terminal
           @param y_p y positive terminal
           @param y_m y negative terminal 
           @param width the width of panel
           @param length the length of panel

        '''  
        self.x_p = x_p
        self.x_m = x_m
        self.y_p = y_p
        self.y_m = y_m
        self.length = length
        self.width = width
        self.x_center = length/2
        self.y_center = width/2
        self.coord_matrix = np.array([[-40, 40], [0, 40], [40, 40], [-40, 0], [0, 0], [40, 0], [-40, -40], [0, -40], [40, -40]])
        self.adc_arr = []
        self.calib_coeff = []
        self.constants = []
        self.offsets = []
        
    def x_scan(self):
        '''@brief scans the x circuit
           @details sets xp terminal to high and the xm terminal to low then sleeps so that the switch can occure then reads teh ADC value
           @return ADC output for the x direction
        '''
        yp = pyb.Pin(self.y_p, pyb.Pin.IN)
        xp = pyb.Pin(self.x_p, pyb.Pin.OUT_PP)
        xm = pyb.Pin(self.x_m, pyb.Pin.OUT_PP)
        ADCym =pyb.ADC(self.y_m)
        utime.sleep_us(10)
        xp.high()
        xm.low()
        utime.sleep_us(10)
        return ADCym.read()


    def y_scan(self):
        '''@brief scans the y circuit
           @details sets yp terminal to high and the ym terminal to low then sleeps so that the switch can occure then reads teh ADC value
           @return ADC output for the y direction

        '''
        yp = pyb.Pin(self.y_p, pyb.Pin.OUT_PP)
        xp = pyb.Pin(self.x_p, pyb.Pin.IN)
        ym = pyb.Pin(self.y_m, pyb.Pin.OUT_PP)
        ADCxm =pyb.ADC(self.x_m)
        utime.sleep_us(10)
        yp.high()
        ym.low()
        utime.sleep_us(10)
        return ADCxm.read()
       
            
    def z_scan(self):
        '''@brief scans the z circuit
           @details sets yp terminal to high and the xm terminal to low then sleeps so that the switch can occure then reads teh ADC value
           @return ADC output for the z direction
        '''
        yp = pyb.Pin(self.y_p, pyb.Pin.OUT_PP)
        xp = pyb.Pin(self.x_p, pyb.Pin.IN)
        xm = pyb.Pin(self.x_m, pyb.Pin.OUT_PP)
        ADCym =pyb.ADC(self.y_m)
        utime.sleep_us(10)
        yp.high()
        xm.low()
        utime.sleep_us(10)
        
    def read_position(self):
        '''@brief reads the position of object in contact with panel 
           @details reads the ADC value at a position and converts it to a milimeter reading 
           @return position of the ball on the touch panel
        '''
        if self.z_scan():      
            self.ADC = np.array([self.x_scan(), self.y_scan()]).transpose()

            return np.dot(self.constants, self.ADC) + self.offsets
            # return (self.x_scan()*self.length/4095 - 88, self.y_scan()*self.width/4095 - 50)
    
    def calibrate(self):
        '''@brief Calibrates the touch panel
           @details Records nine ADC values at specified locations then returns calibration coefficients Kxx, Kxy, Kyx, Kyy, Xc, Yc
           @return the calibration coefficients
        '''
        index = 0 
        print('Please calibrate the touch panel')
        while(index < 9):
            if self.z_scan():
                
                self.adc_arr.append(self.x_scan())
                self.adc_arr.append(self.y_scan())
                self.adc_arr.append(1)
                index += 1
                print('point ', index)
                utime.sleep_ms(100)
        arr = np.array(self.adc_arr)
        mtr = arr.reshape((9,3))
        mtr_T = mtr.transpose()
        print('calibration completed')
        
        self.calib_coeff = np.dot(np.dot(np.linalg.inv(np.dot(mtr_T, mtr)),mtr_T),self.coord_matrix) #linear algebra to find B matrix        
        self.constants = np.array([[self.calib_coeff[0, 0], self.calib_coeff[1, 0]],[self.calib_coeff[0, 1], self.calib_coeff[1, 1]]])    
        self.offsets = np.array([self.calib_coeff[2, 0], self.calib_coeff[2, 1]]).transpose() 
    
        return self.calib_coeff

    def read_coeff(self, cal_values):
        '''@brief reads calibration coefficients from the touch panel
           @details A 3x2 array containing offsets and multplicitive constants is decomposed into constants and offsets
           @param cal_values array of offsets
        '''
        self.constants = np.array([[cal_values[0], cal_values[1]], [cal_values[2], cal_values[3]]])                        
        self.offsets = np.array([cal_values[4], cal_values[5]]).transpose() 
        
            
    
