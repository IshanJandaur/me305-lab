''' @file       fp_task_motor.py
    @brief      Task that interfaces with motor driver
    @details    Task that sets duty cycle from user input and also ensures that the motor doesn't exceed the saturation limit.
    @author     Ishan Jandaur
    @author     Soren Barclay
    @date       December 9, 2021
'''

import utime

class Task_Motor():
     '''@details object of this class is initiated to interface with motor driver
     '''
     def __init__(self, motor_obj, duty_cycle):
        '''@brief constructor of class Task_Motor
           @details takes in critical parameters to run task_motor
           @params duty_cycle: duty cycle of the motor
                   motor_obj: creates object from motor driver class
        '''
        self.duty_cycle = duty_cycle
        self.motor_obj = motor_obj
        self.period = 10000 
        self.sat_limit = 75
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)

     def run(self):
        ''' @brief runs task motor
            @details determines if the duty cycle is above the saturation limit.
            If the duty cycle is above the saturation limit, then the duty cycle is set to the saturation limit. 
        '''
        current_time = utime.ticks_us()
                 
        if (utime.ticks_diff(current_time, self.next_time) >= 0): 
            if self.duty_cycle.read() > self.sat_limit:  
               self.duty_cycle.write(self.sat_limit) 
               
            elif self.duty_cycle.read() < -1*self.sat_limit:
                self.duty_cycle.write(-1*self.sat_limit) 
                
            
            self.motor.set_duty(self.duty_cycle.read())
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
            