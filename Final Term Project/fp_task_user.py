''' @file       fp_task_user.py
    @brief      Task to prompt user for direction
    @details    User is prompted to enter a key on keyboard to conduct a certain action.
                They can either collect data or balance a ball on a platform.
    @author     Ishan Jandaur
    @author     Soren Barclay
    @date       December 9, 2021
'''

import utime, pyb
from micropython import const
import math
import array as arr

## State 0 of the user interface task
S0 = const(0)
## State 1 of the user interface task
S1 = const(1)
## State 2 of the user interface task
S2 = const(2)
## State 3 of the user interface task
S3 = const(3)
## State 4 of the user interface task
S4 = const(4)

class Task_User():
    ''' @brief      Class that interfaces with other tasks
        @details    task that prompts user to either direct hardware to balance ball or collect data corresponding
                    to the ball balancing.
    '''
    
    def __init__(self, controller_state):
        ''' @brief constructor for class
            @details constructor that opens serial port to communicate with keyboard and initates a controllerstate
            @param controller_state: flag that tells program to run controller task

        '''
        self.period = 10000
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The state to run on the next iteration of the finite state machine
        self.state = S0
        self.controller_state = controller_state
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)  
        
    def run(self):
        ''' @brief Runs the task
            @details if time less than the period has elapsed then the task runs. 
            The user is prompted to enter a character to either balance ball or collect data.  
            Once a character is entered the specific functionality is carried out
        '''
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0:
                self.state = S1
                print('''
                      
Welcome to the ball balancer.

press 'b' to begin balancing
press 'd' to begin data collection
                
                      ''')
                       
            elif self.state == S1: # Initially checking for input characters
            
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    
                    if(self.char_in == 'b'):
                        self.controller_state.write(1)
                        print('Balancing beginning. Place the ball in the middle of the platform')
                        
                    elif(self.char_in == 'd'):
                        print('data collection beginning')  

            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)