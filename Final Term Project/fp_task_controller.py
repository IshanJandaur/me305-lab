''' @file       fp_task_controller.py
    @brief      Task that implements controller driver
    @details    Task that implements control system to constantly update duty cycle of the motors based on the position/velocity of the ball and the 
                angular position/velocity of the platform. Homeworks 2 and 3 show the system we modeled and based our gain values from. 
    @author     Ishan Jandaur
    @author     Soren Barclay
    @date       December 9, 2021
'''

from ulab import numpy as np
from micropython import const
import utime

S0 = const(0)
S1 = const(1)

class Task_Controller():
    '''@details Object of this class is created to run control feedback on motors 
    '''
    def __init__(self, x, thetay, dx, dthetay, y, thetax, dy, dthetax, duty_cycle1, duty_cycle2, controller_state):
        '''@brief constructor that initiates critical parameters
           @details constructor that uses data from the IMU and touch panel that are stored in a shares file
           @param x x position of ball on touch panel
           @param thetay rotation of platform relative to y axis
           @param dx x velocity of ball on touch panel 
           @param dthetay angular rotation of platform relative to y axis
           @param y y position of ball on touch panel
           @param thetax rotation of platform relative to x axis
           @param dy y velocity of ball on touch panel
           @param dthetax angular rotation of platform relative to x axis
           @param duty_cycle1 duty cycle needed for motor 1
           @param duty_cycle2 duty cycle needed for motor 2
           @param controller_state flag to create an object from Task_Controller
        '''   
        self.x = x
        self.thetay = thetay
        self.dx = dx
        self.dthetay = dthetay
        
        self.y = y
        self.thetax = thetax
        self.dy = dy
        self.dthetax = dthetax
        
        self.kx = np.array([40, 14, 57.5, 1.5]) 
        self.ky = np.array([-60, -15, -70, -2.2]) 
        self.state = S0
        self.motor_gain = 33.36
        self.duty_cycle1 = duty_cycle1
        self.duty_cycle2 = duty_cycle2
        
        self.controller_state = controller_state
    
    def run(self):   #Takes matrix of [x, theta, xdot, thetadot], multiplies it by k gain, multiply by motor torque gain, then writes to duty cycle
        '''@brief function that runs feedback control
           @details Takes matrix of state vector, [x, theta, xdot, thetadot] and multiplies it by k gain values.
           Multiply by motor torque gain (from spec sheet), then writes to duty cycle shares file. 
        '''
        if self.controller_state.read() == 1:
            
            self.measx = np.array([self.x.read(), self.thetay.read(), self.dx.read(), self.dthetay.read()]).transpose()
            self.measy = np.array([self.y.read(), self.thetax.read(), self.dy.read(), self.thetax.read()]).transpose()
            self.trqx = np.dot(self.kx, self.measx)
            self.trqy = np.dot(self.ky, self.measy)
            
            self.duty_cycle1.write(self.motor_gain*self.trqx) # motor 1 faces in negative x direction
            self.duty_cycle2.write(self.motor_gain*self.trqy) # motor 2 faces in positive y direction
            


  
            

            
            
            
            
        
        
        
        
        
        
        
