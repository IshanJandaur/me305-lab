''' @file       fp_task_panel.py
    @brief      Panel task that interfaces with panel driver
    @details    Calibrates the touch panel and also uses alpha and beta filtering to obtain velocity from the touch panel
    @author     Ishan Jandaur
    @author     Soren Barclay
    @date       December 9, 2021
'''

import os
from micropython import const
import utime

S0 = const(0)
S1 = const(1)


class Task_panel():
    '''@details interfaces with panel driver to obtain critical values that will be stored in shared files
       and used by other task programs
    '''
    def __init__(self, panel_drv, x, dx, y, dy):
        '''@brief constructor for Task_Panel class
           @details Initiates critical parameters that will be used for various calculations
           @param panel_drv: create instance of Panel Driver class
           @param x: x position of ball in x direction
           @param dx: velocity of ball in x direction
           @param y: y position of ball in y direction
           @param dy: velocity of ball in y direction 
        '''
        
        self.panel_drv = panel_drv
        self.x = 0
        self.xhat = 0
        self.vxhat = 0
        self.yhat = 0
        self.vyhat = 0
        self.alpha = 0.85
        self.beta = 0.005
        self.state = S0
        self.period = 10000 
        self.delta = 0
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        self.last_time = utime.ticks_us()
        self.current_time = 0
        self.pos = None
        self.measx = x
        self.measdx = dx
        self.measy = y
        self.measdy = dy

    def run(self): 
        '''@brief Calibration proecdure is ran before obtaining velocities
           @details Calibration procedure looks for a file on nucleo that has calibration coefficients stored. If calibration file does not exist
           calibration will happen manually and coefficients will be written to a file that is stored on the Nucleo. After calibration coefficients
           are obtained, state 1 is entered. In state 1, linear velocity is read by determining the time between each position reading. The difference between position readings
           is divided by teh time elasped to get velocities. 
        '''
        
        current_time = utime.ticks_us()
                 
        if (utime.ticks_diff(current_time, self.next_time) >= 0):   

            if self.state == S0: #Calibration 
                filenameTP = "RT_cal_coeffs.txt"
                if filenameTP in os.listdir(): #File exists, read from it
                    with open(filenameTP, 'r') as f: #Read the first line of the file
                        cal_string = f.readline()
                        cal_values = [float(cal_value) for cal_value in cal_string.strip().split(',')]  #Split the line into multiple strings and then convert each one to a float
                        self.panel_drv.read_coeff(cal_values)
    
                else: #File doesnt exist, calibrate manually and write the coefficients to the file
                    with open(filenameTP, 'w') as f:
                        #Perform manual calibrate 
                        arr = self.panel_drv.calibrate()
                        (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = (arr[0, 0], arr[1, 0], arr[0, 1], arr[1, 1], arr[2, 0], arr[2, 1])
                        #Then, write the calibration coefficients to the file as a string
                        f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")     
                
                self.state = S1
                        
            elif self.state == S1: #Reading linear velocity
                
                self.pos = self.panel_drv.read_position()
                
                if self.pos is not None:
                    self.delta = utime.ticks_diff(self.current_time,self.last_time)/10**6

                    self.x = self.pos[0]/1000
                    self.xhat = self.xhat + self.alpha*(self.x - self.xhat) + self.delta*self.vxhat
                    self.vxhat = self.vxhat + self.beta*(self. x -self.xhat)/self.delta
                    self.measx.write(self.xhat)
                    self.measdx.write(self.vxhat)
                    
                    
                    self.y = self.pos[1]/1000
                    self.yhat = self.yhat + self.alpha*(self.y - self.yhat) + self.delta*self.vyhat
                    self.vyhat = self.vyhat + self.beta*(self.y - self.yhat)/self.delta
                    self.measy.write(self.yhat)
                    self.measdy.write(self.vyhat)
                    
                else:
                    self.measx.write(0)
                    self.measdx.write(0)
                    self.measy.write(0)
                    self.measdy.write(0)
                    
                self.last_time = utime.ticks_us()
            

                
                
                
    



                    


