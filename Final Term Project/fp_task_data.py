''' @file       fp_task_data.py
    @brief      Collects data and prints it out to the user
    @details    Prints out the touch panel reading readings and angle of the platform so that user sees its relative position
                The corresponding duty cycles of each of the motors is also shown.
    @author     Ishan Jandaur
    @author     Soren Barclay
    @date       December 9, 2021
'''

import arr
import utime

class Task_Data():
    ''' @brief      Class that collects data for final term project
        @details    Class that reads out critical measurements aligned with teh controller design of the final term project.
                    Reads out the position of the ball on the touch panel and the position of the platform with the corresponding duty cycles.
                    It can be used as a trouble-shooting tool for controller design and can also justify if proper control feedback is occurring.
    '''
    
    def __init__(self, x, thetay, y, thetax, duty_cycle1, duty_cycle2):
        ''' @brief constructor that declares parameters used to run data collection
        
            @details values from the IMU and touch panel are stored as shares and read within this class
                     With task_data we are able to print out critical values of the motor that are used to 
                     determine the duty_cycle for each motor. This data can be used to show our control 
                     system working and each of the positions/velocities reaching steady-state.
            
            @param x the x value of the ball on the touch panel
            @param y the y value of the ball on the touch panel
            @param thetax the rotation of the platform about the x-axis
            @param thetay the rotation of the platform about the y-axis
            @param duty_cycle1 the duty cycle calculated for motor 1
            @param duty_cycle2 the duty cycle calculated for motor 2
            '''
        self.x = x
        self.thetay = thetay
        self.y = y
        self.thetax = thetax
        self.duty_cycle1 = duty_cycle1
        self.duty_cycle2 = duty_cycle2
       
       #check the data type of each array 
       
        self.xarr = arr.array('i', range(1000))
        self.yarr = arr.array('i', range(1000))
        self.thetaxarr = arr.array('i', range(1000))
        self.thetayarr = arr.array('i', range(1000))
        self.duty1arr = arr.array('f', range(1000))
        self.duty2arr = arr.array('f', range(1000))
        self.timarr = arr.array('f', range(1000))
        
    def run(self):
        '''@brief runs the data collection for 10 seconds total
           @details Each of the critical parameters are read from the shares file and added to their respective list
                    After 10 seconds is reached or the data collection is stopped by the user, each of the lists is indexed through
                    and printed out to the user in the form of comma separated values that can be inputed into an excel file and plotted
        '''
        
        if utime.ticks_ms() - self.start_time <= 10000:
                if self.char_in == 'S' or self.char_in == 's': #clear is called
                    print('Data collection ended prematurely')
                    self.start_time = -10000 #regardless of the current utime.ticks time it will be above 30,000 and the list will be printed

                elif self.char_in == 'g':
                    
                    #motor 1 causes the platform to rotate about the y and the ball to move in the x direciton
                    #position should be reading out the x position of the ball
                    #angle should be reading out the angle of the platform
                    #duty should be reading out the duty cycle calculated 

                    self.xarr[self.index] = self.x.read()
                    self.yarr[self.index] = self.y.read()
                    self.thetaxarr[self.index] = self.thetax.read()
                    self.thetayarr[self.index] = self.thetay.read()
                    self.duty1arr[self.index] = self.duty_cycle1.read()
                    self.duty2arr[self.index] = self.duty_cycle2.read()
                    self.timarr[self.index] = utime.ticks_ms() - self.start_time
                
                    self.index += 1

        else:
                
            if self.counter == 0: #Once collection has ended, data is printed to console
                print('Time: Ball X Position: Ball Y Position: Platform X Angle: Platform Y Angle: Motor 1 Duty Cycle: Motor 2 Duty Cycle:')
                print('{:},    {:},     {:},     {:},    {:},     {:},    {:}'.format(self.timarr[self.counter], self.xarr[self.counter],self.yarr[self.counter], self.thetaxarr[self.counter], self.thetayarr[self.counter], self.duty1arr[self.counter], self.duty2arr[self.counter]))
                self.counter += 1
                
                
            if self.counter > self.index - 1:
                self.char_in = ''
                self.timarr = range(1000)
                self.xarr = range(1000)
                self.yarr = range(1000)
                self.thetaxarr = range(1000)
                self.thetayarr = range(1000)
                self.duty1arr = range(1000)
                self.duty2arr= range(10000)
                self.counter = 0
                self.index = 0
                    