''' @file       fp_task_IMU.py
    @brief      the IMU interacts with the controller and writes to necessary shared variables
    @details    The IMU instantiates, variables, checks if calibration is necessary, then reads IMU data
    @author     Ishan Jandaur
    @author     Soren Barclay
    @date       December 9, 2021
'''

import os
from micropython import const
import utime
import struct
from ulab import numpy as np
import math


S0 = const(0)
S1 = const (1)

class Task_IMU():
    ''' @file       task_IMU
        @brief      IMU data is recorded and then shared 
        @details    The IMU records then data then writes to shared variables
    '''

    
    def __init__(self, IMU_drv, thetax, dthetax, thetay, dthetay): 
        ''' @brief      IMU instantiates the driver from main and four shared variables
            @details    The IMU records then data then writes to shared variables
            @param IMU_drv: the IMU driver
            @param thetax: shared theta variable
            @param dthetax: shared thetadot variable
            @param thetay: shared theta variable
            @param dthetay: shared thetadot variable
        '''
        
        self.state = S0 
        self.IMU_drv = IMU_drv
        self.thetax = thetax
        self.dthetax = dthetax
        self.thetay = thetay
        self.dthetay = dthetay
        self.period = 10000
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        
    def run(self):
        ''' @brief      IMU task is run
            @details    The IMU checks to see if calibration has occured by writing and reading to a file 
                        if calibration has occured then the state is changed and IMU values are written to 
                        appropriate shared variables. The angles are square rooted to normalize the difference 
                        between small and large degrees of the platform
        '''
        
        
        current_time = utime.ticks_us()
                 
        if (utime.ticks_diff(current_time, self.next_time) >= 0):   
            
            if self.state == S0:
                self.IMU_drv.change_mode(12) #NDOF
                
                filenameIM = 'IMU_cal_coeffs.txt'
                if filenameIM in os.listdir():
                    with open(filenameIM, 'r') as f: #File exists, read from it
                        cal_data_string = f.readline()
                        # print(cal_data_string)
                        cal_values = [int(cal_value, 16) for cal_value in cal_data_string.strip().split(',')]                 
                        buf = bytearray(cal_values)
                        print(buf)
                        self.IMU_drv.write_coeff(buf)

                else: #File doesnt exist, calibrate manually and write the coefficients to the file
                    with open(filenameIM, 'w') as f:
                        self.IMU_drv.calibrate()
                        coeff = self.IMU_drv.read_coeff()
                        str1 = ",".join([hex(index) for index in coeff])
                        print(str1)
                        f.write(f"{str1}\r\n")     
                        
                        
                self.state = S1   
                    
            elif self.state == S1:
                
                theta = self.IMU_drv.read_euler()
                dtheta = self.IMU_drv.read_ang_vel()
                
                factor = 0.32 
                
                if theta[1] > 0:
                    self.thetax.write(math.sqrt((abs(theta[1]*3.141592/180)))*factor)
    
                if theta[1] <= 0:
                    self.thetax.write(math.sqrt(abs(theta[1]*3.141592/180))*-1*factor)
                
                if theta[2] > 0:
                    self.thetay.write(math.sqrt((abs(theta[2]*3.141592/180)))*factor)      
                
                if theta[2] <= 0:
                    self.thetay.write(math.sqrt(abs(theta[2]*3.141592/180))*-1*factor)                    
                
                if dtheta[1] > 0:
                    self.dthetax.write(math.sqrt((abs(dtheta[1]*3.141592/180)))*-1*factor)
    
                if dtheta[1] <= 0:
                    self.dthetax.write(math.sqrt(abs(dtheta[1]*3.141592/180))*factor)
                
                if dtheta[2] > 0:
                    self.dthetay.write(math.sqrt((abs(dtheta[2]*3.141592/180)))*factor)      
                
                if dtheta[2] <= 0:
                    self.dthetay.write(math.sqrt(abs(dtheta[2]*3.141592/180))*-1*factor) 
                
                
                
                
                