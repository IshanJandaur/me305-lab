''' @file       fp_IMU_driver.py
    @brief      IMU driver communicate directly with the IMU to read angles and angular velocities 
    @details    The driver can calibrate, change modes, and read out angles and angular velocities
    @author     Ishan Jandaur
    @author     Soren Barclay
    @date       December 9, 2021
'''

from pyb import I2C
import struct
import utime
import os

class BNO055:
    '''@brief Driver that interacts with IMU to display orientation
       @details Creates an I2C bus with the IMU to exchange data
    '''

    def __init__ (self):
        '''@brief Instantiates important variablwes
           @details the I2C bus is instantiated on channel 1 and the nucleo is the master
        '''
        self.i2c = I2C(1, I2C.MASTER)    
        
    def change_mode(self, address):
        '''@brief changes the mode of the IMU
           @details changes the mode of the IMU between NDOF or configuration at the given address 0x28 -nucleo on the  
        '''
        self.i2c.mem_write(address, 0x28,0x3D)
        
    def calib_status(self):
        '''@brief reads out calibration values
           @details a list of four variables that should all read three when calibrated
           @return returns the calibration 
        '''
        self.cal_bytes = self.i2c.mem_read(bytearray(1), 0x28, 0x35)
        self.cal_status = (self.cal_bytes[0] & 0b11,
                          (self.cal_bytes[0] & 0b11 << 2) >> 2,
                          (self.cal_bytes[0] & 0b11 << 4) >> 4,
                          (self.cal_bytes[0] & 0b11 << 6) >> 6)
       
        return self.cal_status
    
    def read_coeff(self): 
        '''@brief reads calibration bytes
           @details reads 22 bytes of calibration bytes which are stored internally
           @return returns the calibration bytes
        '''
        return self.i2c.mem_read(22, 0x28, 0x55)
        
    def write_coeff(self, coeff):
        '''@brief  writes 22 bytes to calibration data
           @details these bytes are stored internally
           @param coeff the coefficients that are written to the IMU
        '''
        self.change_mode(0) #config
        self.i2c.mem_write(coeff, 0x28, 0x3D)
        self.change_mode(12) #NDOF
        
    def read_euler(self):
        '''@brief  reads angles from the IMU
           @details heading, pitch, and roll are stored in a tuple with length 3 
           @return returns a tuple with heading, pitch, and roll
        '''
        eul_bytes = self.i2c.mem_read(bytearray(6), 0x28, 0x1A)
        eul_signed_ints = struct.unpack('<hhh', eul_bytes)
        eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
        return eul_vals
    def read_ang_vel(self):
        '''@brief  reads angular velocities from the IMU
           @details heading, pitch, and roll angular velocities are stored in a tuple
           @return these velocities are returned
        '''
        eul_velocities = self.i2c.mem_read(bytearray(6), 0x28, 0x14)
        eul_signed_vel = struct.unpack('<hhh', eul_velocities)
        eul_valrates = tuple(index/16 for index in eul_signed_vel)
        return eul_valrates
    
    def calibrate(self):
        '''@brief  calibrates the IMU
           @details the mode is changed into NDOF then the calibration values are recorded
        '''
        utime.sleep_ms(10)
        calib = self.calib_status()
        state = 0
        print('printing calibration values')
        while state == 0:
            utime.sleep_ms(10)
            print(calib)
            if(calib[0]*calib[1]*calib[2]*calib[3] == 81): 
                print('Sensor Calibrated')
                state = 1
            
        
    
    
    