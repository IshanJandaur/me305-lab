''' @file       main_lab02.py
    @brief      Main file that instantiates objects and runs them in a loop
    @details    Three shares abojects are created for the task encoder and task
                user to communicate with each other. These tasks use the shares 
                as input parameters. The program runs until the user presses 
                Ctrl+C
'''

import pyb
import shares
import task_user
import task_encoder
import encoder_driver
import task_motor
import motor_driver
import controller_driver

def main():
    ''' @brief The main program
        @details The function instantiates three share variables, 
        a user class, and an encoder class then runs the two tasks until an 
        interrupt is recieved
    '''

    position1 = shares.Share(0)
    position2 = shares.Share(0)
    
    delta1 = shares.Share(0)
    delta2 = shares.Share(0)

    zero1 = shares.Share(False)
    zero2 = shares.Share(False)
    
    duty_cycle1 = shares.Share(0)
    duty_cycle2 = shares.Share(0)
    
    kp = shares.Share(0)
    reference1 = shares.Share(0)
    reference2 = shares.Share(0)
    sat_limit = shares.Share(0)
    
    
    clr_flt = shares.Share(False)
    
    ## Encoder driver objects
    encoder_drv1 = encoder_driver.Encoder(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)
    encoder_drv2 = encoder_driver.Encoder(8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7)
    
    ## Task encoder objects
    encoder_tsk1 = task_encoder.Task_Encoder(encoder_drv1, position1, delta1, zero1)
    encoder_tsk2 = task_encoder.Task_Encoder(encoder_drv2, position2, delta2, zero2)
    
    ## Motor Driver objects
    motor_drv = motor_driver.DRV8847(pyb.Pin.cpu.A15, pyb.Pin.cpu.B2)
    motor_1 = motor_drv.motor(pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2)
    motor_2 = motor_drv.motor(pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4)  
    
    ## Controller driver
    control_drv1 = controller_driver.ClosedLoop(reference1, delta1, kp, sat_limit, duty_cycle1)
    control_drv2 = controller_driver.ClosedLoop(reference2, delta2, kp, sat_limit, duty_cycle2)
    
    ## Task motor objects
    motor_tsk1 = task_motor.Task_Motor(control_drv1, motor_1, motor_drv, duty_cycle1, clr_flt)
    motor_tsk2 = task_motor.Task_Motor(control_drv2, motor_2, motor_drv, duty_cycle2, clr_flt)
    
    
    ## An user interface task object
    user = task_user.Task_User(position1, position2, delta1, delta2, zero1, zero2, duty_cycle1, duty_cycle2, clr_flt, kp, reference1, reference2)
   
    ## A list of tasks to run
    task_list = [user, encoder_tsk1, encoder_tsk2, motor_tsk1, motor_tsk2]
    
    while(True):
        try:
            for task in task_list:
                task.run()
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')
    
if __name__ == '__main__':
    main()

