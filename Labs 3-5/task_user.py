''' @file       task_user.py
    @brief      User interface task for cooperative multitasking example.
    @details    Implements a finite state machine
'''

import utime, pyb
from micropython import const
import math
import array as arr

## State 0 of the user interface task
S0 = const(0)
## State 1 of the user interface task
S1 = const(1)
## State 2 of the user interface task
S2 = const(2)
## State 3 of the user interface task
S3 = const(3)
## State 4 of the user interface task
S4 = const(4)

class Task_User():
    ''' @brief      User interface task for cooperative multitasking example.
        @details    Implements a finite state machine
    '''
    
    def __init__(self, position1, position2, delta1, delta2, zero1, zero2, duty_cycle1, duty_cycle2, clr_flt, kp, reference1, reference2):
        ''' @brief              Constructs an LED task.
            @details            The LED task is implemented as a finite state
                                machine.
        '''
        self.period = 100000
        
        self.position1 = position1
        self.position2 = position2
        
        self.delta1= delta1
        self.delta2= delta2
        
        self.zero1= zero1
        self.zero2= zero2
        
        self.duty_cycle1 = duty_cycle1
        self.duty_cycle2 = duty_cycle2
        
        self.clr_flt = clr_flt
        
        self.kp = kp
        self.reference1 = reference1
        self.reference2 = reference2
        

        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The state to run on the next iteration of the finite state machine
        self.state = S0
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        self.char_in = ''
        self.my_str = ''
        self.counter = 0
        self.index = 0
        self.iteration = 0

        self.timarr = arr.array('i', range(3000))
        self.posarr = arr.array('f', range(3000))
        self.velarr = arr.array('f',range(3000))
        self.actarr = arr.array('f',range(3000))

    
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
        '''
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0:
                self.state = S1
                print('''
                      
Welcome to encoder controller.
                
press z to zero out the encoder 1
press Z to zero out the encoder 2

press p to print out the position of encoder 1
press P to print out the position of encoder 2

press d to print out the delta for encoder 1
press D to print out the delta for encoder 2

press m to enter a duty cycle for motor 1
press M to enter a duty cycle for motor 2

press c or C to clear a fault condition in the motor

press g to collect data for encoder 1 for 30 seconds and return a csv
press G to collect data for encoder 2 for 30 seconds and return a csv

press 1 to begin closed loop control on motor 1
press 2 to begin closed loop control on motor 2

press s or S to end data collection prematurely
                
                      ''')
                       
            elif self.state == S1: #checking for input characters
            
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                    
                    if(self.char_in == 'z'):
                        print('position has been zeroed')
                        self.zero1.write(True)
                    elif(self.char_in == 'Z'):
                        print('position has been zeroed')  
                        self.zero2.write(True)
                        
                    elif(self.char_in == 'p'):
                        print('Reading position 1')
                        print(self.position1.read())
                    elif(self.char_in == 'P'):
                        print('Reading position 2')
                        print(self.position2.read()) 
                        
                    elif(self.char_in == 'd'):
                        print('Reading delta')
                        print(self.delta1.read())
                    elif(self.char_in == 'D'):
                        print('Reading delta')
                        print(self.delta2.read()) 
                        
                    elif(self.char_in == 'g' or self.char_in == 'G'): 
                        print('Collecting encoder data')
                        self.start_time = utime.ticks_ms()
                        self.state = S2
                    
                    elif(self.char_in == 'm' or self.char_in == 'M'):
                        print('Enter a duty cycle for motor')                       
                        self.state = S3 
                        self.my_str = ''
                        
                    elif(self.char_in == 'c' or self.char_in == 'C'):
                        print('Motor fault cleared')
                        self.clr_flt.write(True)
                    
                    elif(self.char_in == '1' or self.char_in == '2'):
                        print('enter a kp value')
                        self.iteration = 1
                        self.state = S3

                        
                    
                        
                    else:
                        print('Command \'{:}\' is invalid.'.format(self.char_in))
                            
            elif self.state == S2: #Collect data 
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()
                                
                if utime.ticks_ms() - self.start_time <= 30000:
                    if self.char_in == 'S' or self.char_in == 's': #clear is called
                        print('Data collection ended prematurely')
                        self.start_time = -30000 #regardless of the current utime.ticks time it will be above 30,000 and the list will be printed
    
                    elif self.char_in == 'g':
                        self.posarr[self.index] = self.position1.read() * (2*math.pi*100/2000)
                        self.velarr[self.index] = self.delta1.read() *(2*math.pi*100/2000)
                        self.timarr[self.index] = utime.ticks_ms() - self.start_time
                    
                        self.index += 1
                        
                    else:
                        self.posarr[self.index] = self.position2.read() * (2*math.pi*100/2000)
                        self.velarr[self.index] = self.delta2.read() *(2*math.pi*100/2000)
                        self.timarr[self.index] = utime.ticks_ms() - self.start_time
                    
                        self.index += 1
                else:
                    
                    if self.counter == 0:
                        print('Time; Position; Delta')
                    
                    print('{:},    {:},     {:}'.format(self.timarr[self.counter],self.posarr[self.counter], self.velarr[self.counter]))
                    self.counter += 1
                    
                    
                    if self.counter > self.index - 1:
                        self.state = S1
                        self.char_in = ''
                        self.timarr = range(3000)
                        self.velarr = range(3000)
                        self.posarr = range(3000)
                        self.counter = 0
                        self.index = 0
                    
            
            elif self.state == S3: #Collecting digits
                if self.ser.any():
                    self.num_in = self.ser.read(1).decode()
                    self.ser.write(self.num_in)
                    if self.num_in.isdigit() == True:
                        self.my_str += self.num_in
                    elif self.num_in == '-':
                        if self.my_str == '':
                            self.my_str += self.num_in
                    elif self.num_in == '\x7F':
                        if self.my_str != '':
                            self.my_str = self.my_str[:-1]
                    elif self.num_in == '\r' or '\n':
                        print('\n')
                        self.state = S1
                        if(self.char_in == 'm'):
                            self.duty_cycle1.write(int(self.my_str))
                        elif(self.char_in == 'M'):
                            self.duty_cycle2.write(int(self.my_str))
                            
                        elif(self.iteration == 1):
                            self.kp.write(int(self.my_str))
                            self.state = S3
                            self.iteration = 2
                            print('Enter a velocity reference')
                            
                        elif(self.char_in == '1' and self.iteration == 2):
                            self.reference1.write(int(self.my_str))
                            self.iteration = 0
                            self.start_time = utime.ticks_ms()
                            self.state = S4
                            
                        elif(self.char_in == '2' and self.iteration == 2):
                            self.reference2.write(int(self.my_str))  
                            self.iteration = 0 
                            self.start_time = utime.ticks_ms()
                            self.state = S4                                       

                        self.my_str = ''
            
            elif self.state == S4: #collect data with a control loop, both motors turn on with duty cycle input, step response not working, weird print out at the end of the data colleciton
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode()

                if utime.ticks_ms() - self.start_time <= 10000:
                    if self.char_in == 'S' or self.char_in == 's': #clear is called
                        print('Data collection ended prematurely')
                        self.start_time = -10000 #regardless of the current utime.ticks time it will be above 30,000 and the list will be printed
                    
                    elif self.char_in == '1':
                        self.timarr[self.index] = utime.ticks_ms() - self.start_time
                        self.velarr[self.index] = self.delta1.read() *(2*math.pi*100/2000) # rad/s
                        self.actarr[self.index] = self.duty_cycle1.read()
                        self.index += 1
                    
                    else:
                        self.timarr[self.index] = utime.ticks_ms() - self.start_time
                        self.velarr[self.index] = self.delta2.read() * (2*math.pi*100/2000)
                        self.actarr[self.index] = self.duty_cycle2.read()
                        self.index += 1
            
                else:
                    if self.counter == 0:
                        print('Time; Speed; Actuation Level')
                    print('{:},    {:},     {:}'.format(self.timarr[self.counter],self.velarr[self.counter], self.actarr[self.counter]))
                    self.counter += 1
                    
                    if self.counter > self.index:
                        self.state = S1
                        self.char_in = ''
                        self.timarr = range(3000)
                        self.velarr = range(3000)
                        self.actarr = range(3000)
                        self.counter = 0
                        self.index = 0
    
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)