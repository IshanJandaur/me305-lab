"""
Created on Thu Oct 28 12:21:12 2021

@author: Ishan
"""

class ClosedLoop:
    
    def __init__(self, reference, measured, K_p, sat_limit, duty_cycle):
        
        self.reference = reference
        self.measured = measured
        self.K_p = K_p
        self.sat_limit = sat_limit
        self.duty_cycle = duty_cycle
        
        
    def update(self):
        
        if self.K_p.read() == 0 or self.reference.read() == 0:
            return
        
        self.duty_cycle.write(int(self.K_p.read()*(self.reference.read() - (self.measured.read() * 2*3.141592356/4000))))

    def get_Kp(self):
        return self.K_p.read()
    
    def set_Kp(self,K_p):
        self.K_p.write(K_p)