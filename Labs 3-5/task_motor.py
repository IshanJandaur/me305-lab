import utime

class Task_Motor():

     def __init__(self, con_object, motor_obj, motor_drv, duty_cycle, clr_flt):
         
        self.duty_cycle = duty_cycle
        self.motor_drv = motor_drv
        self.motor = motor_obj
        self.period = 10000 
        self.clr_flt = clr_flt
        self.con_object = con_object
        
        # Enable the motor driver
        self.motor_drv.enable()
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)

     def run(self):
        ''' @brief 
            @details 
        '''
        current_time = utime.ticks_us()
                 
        if (utime.ticks_diff(current_time, self.next_time) >= 0):                 
            
            # Set the duty cycle of the first motor to 40 percent and the duty cycle of
            # the second motor to 60 percent
            self.motor.set_duty(self.duty_cycle.read())
                
            self.next_time = utime.ticks_add(self.next_time, self.period)
            
            if self.clr_flt.read() == True:
                self.motor_drv.enable()
                self.clr_flt.write(False)
                
            # Update closed loop controller   
            self.con_object.update()
            