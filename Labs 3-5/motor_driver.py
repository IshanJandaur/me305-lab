''' @file motor_driver.py
'''

import pyb
import utime

class DRV8847:
    ''' @brief A motor driver class for the DRV8847 from TI.
    @details Objects of this class can be used to configure the DRV8847
    motor driver and to create one or more objects of the
    Motor class which can be used to perform motor
    control.
    
    Refer to the DRV8847 datasheet here:
    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__ (self, sleepPin, faultPin):
        ''' @brief Initializes and returns a DRV8847 object.
        '''
        self.pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
        self.sleep_pin = pyb.Pin(sleepPin, pyb.Pin.OUT_PP)
        self.fault_pin = pyb.Pin(faultPin, pyb.Pin.OUT_PP)
        self.ext_int = pyb.ExtInt(self.fault_pin, mode=pyb.ExtInt.IRQ_FALLING,
                                  pull=pyb.Pin.PULL_DOWN, callback=self.fault_cb)

    def enable (self):
        ''' @brief Brings the DRV8847 out of sleep mode.
        
        '''
        self.ext_int.disable()
        self.sleep_pin.high()
        utime.sleep_us(25)
        self.ext_int.enable()
    
    def disable (self):
        ''' @brief Puts the DRV8847 in sleep mode.
        '''
        self.sleep_pin.low()
    
    def fault_cb (self, IRQ_src):
        ''' @brief Callback function to run on fault condition.
        @param IRQ_src The source of the interrupt request.
        '''
        self.disable()
        print('program terminating')
    
    def motor (self, pinA, pinB, chA, chB):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
        @return An object of class Motor
        '''
        return Motor(pinA, pinB, chA, chB)

class Motor:
    ''' @brief A motor class for one channel of the DRV8847.
    @details Objects of this class can be used to apply PWM to a given
    DC motor.
    '''

    def __init__ (self, pin_A, pin_B, channel_A, channel_B):
        ''' @brief Initializes and returns a motor object associated with the DRV8847.
        @details Objects of this class should not be instantiated
        directly. Instead create a DRV8847 object and use
        that to create Motor objects using the method
        DRV8847.motor().
        '''
        tim3 = pyb.Timer(3, freq = 20000)
        pin1 = pyb.Pin(pin_A, pyb.Pin.OUT_PP)
        pin2 = pyb.Pin(pin_B, pyb.Pin.OUT_PP)
        self.tchA = tim3.channel(channel_A, pyb.Timer.PWM, pin=pin1)
        self.tchB = tim3.channel(channel_B, pyb.Timer.PWM, pin=pin2)

        
    
        pass
    
    def set_duty (self, duty):
        ''' @brief Set the PWM duty cycle for the motor channel.
        @details This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed number holding the duty
        cycle of the PWM signal sent to the motor
        '''
        if duty >= 0: #forward
            self.tchA.pulse_width_percent(duty)
            self.tchB.pulse_width_percent(0)
        else: #backward
            duty = abs(duty)
            self.tchA.pulse_width_percent(0)
            self.tchB.pulse_width_percent(duty)
            
    
