''' @file main.py
    @brief Prompts the user to push a button then switches between three wave forms square, sawtooth, and sin(x) until the program is exited
    @details The program displays its input message then waits for an interrupt from pin C13. Each time the main while loop runs, 
    the update_timer function is called. This constantly updates the time value and therefore the pin brightness associated with that time value.
    Each time the button is pressed, the timer is reset to 0. The program switches between these waveforms indefinitely until a keyboard interrupt 
    is given ctl+C or the program is closed manually.
    @author Ishan Jandaur
    @author Soren Barclay
    @date
    @copyright
'''

import pyb
import utime
import math

def onButtonPressFCN(IRQ_src):
    '''
    @brief Interrupt function that reassigns button value
    @details This function is constantly waiting through the loop for the button to be pushed. 
    Prior to the function running button is set to False. 
    Once an interrupt is given at PC13, global variable button is reassigned to true.
    @param IRQ_SRC
    '''
    global button
    button = True

def reset_timer():
    '''
    @brief resets the timer value to current time
    @details this function is called evverytime the button is pressed. 
    It resets the start point in which we are subtracting from the next time. 
    It sets a zero point for the given wave form
    '''
    global start
    start = utime.ticks_ms()

def update_timer(start):
    '''
    @brief constantly updates the elapsed time
    @details at the beginning of every iteration of the while loop, the time is updated with respect to the given start point. 
    This is accomplished by subtracting the current time from the start time 
    @param start
    @return utime.ticks_diff(end,start)/1000
    '''
    end = utime.ticks_ms()
    return utime.ticks_diff(end,start)/1000

def update_STW(elapsed):
    '''
    @brief This function is used to calculate the value of LED brightness for the sawtooth waveform
    @details elapsed represents miliseconds and the modulus operator is done to it to get a decimal value in return. This decimal value correlates with a certain brightness value.
    The result of the modulus operator is multiplied by 100 and thus it creates a linear increase in LED brigthness until it reaches 100%
    @param elapsed- refers to difference between start time adn end time in miliseconds. start time is 0 and is distinguished in reset_timer function
    @return (elapsed % 1)*100

    '''
    return (elapsed % 1)*100

def update_PWM(elapsed):
    '''
    @brief This function is used to calculate the value for the square waveform
    @details Conducting the modulus operator on the time value gives you a remainer ranging from 0.1 to 1.0
    Whenever the result is below 0.5, the light does not turn on. When the result is above 0.5, the light turns on. 
    Since time is counted consistently, the light is on and off for the same amount of time. 
    @param elapsed - refers to difference between start time adn end time in miliseconds. start time is 0 and is distinguished in reset_timer function
    @return (elapsed % 1 < 0.5)
    '''
    return (elapsed % 1 < 0.5)

def square_wave(elapsed):
    '''
    @brief updates LED brightness basedon time variable to cycle light through square pattern
    @details calls on the update_PWM function and uses if statements to determine if the value from update_PWM makes the light turn on or not
    @param elapsed - refers to difference between start time adn end time in miliseconds. start time is 0 and is distinguished in reset_timer function
    '''
    if(update_PWM(elapsed)):
        t2ch1.pulse_width_percent(100)
    else:
        t2ch1.pulse_width_percent(0)
def sawtooth(elapsed):
    '''
    @brief updates LED brightness based on time variable to cycle light through sawtooth pattern
    @details calls on the update_STW function to determine what the brightness value should be at a designated time 
    @param - refers to difference between start time adn end time in miliseconds. start time is 0 and is distinguished in reset_timer function
    @return 
    '''
    t2ch1.pulse_width_percent(update_STW(elapsed))

def sine_Wave(elapsed):
    '''
    @brief updates LED brightness basedon time variable to cycle light through sine pattern
    @details calculates the brightness value using the python math library built in funciton sin.
    The python sin() function calculates the sin of given value in radians.
    This value is multiplied by 50 to scale up so that the sine wave amplitude is 1 and ranges from 0 to 2(pi).
    @param - refers to difference between start time adn end time in miliseconds. start time is 0 and is distinguished in reset_timer function
    @return 
    '''
    t2ch1.pulse_width_percent(50*math.sin(elapsed))


#initializing variables
i = 0
button = False
pinC13 = pyb.Pin (pyb.Pin.cpu.C13, pyb.Pin.OUT_PP)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

if __name__ == '__main__':
   
  
 #introductory print statement   
    print('''
          
          Hello user, welcome to lab 1, Getting Started with Hardware. 
          This program will allow you to implement a finite state machine on 
          Nucleo.
          It will allow you to cycle through 3 different LED pulse patterns.
          
          Every time you hit the blue button you will
          cycle through a square, sine, and sawtooth blinking patterns.
          
          In order to start the program hit the blue button.''')

    start = utime.ticks_ms()
    
    while(True):
        
        elapsed = update_timer(start)
        
        if(i == 0):
            if(button == True):
                print('Square Wave')
                i = 1
                button = False
                continue           
             
        elif(i == 1):
            #run state 1
            square_wave(elapsed)               
            if(button == True):
                print('Sawtooth')
                i = 2
                button = False
                reset_timer()
                continue           
 
        elif(i == 2):
            #run state 2
            sawtooth(elapsed)
            if(button == True):
                print('Sine Wave')
                i = 3
                button = False
                reset_timer()
                continue 

        elif(i == 3):
            #run state 3
            t2ch1.pulse_width_percent(100*math.sin(elapsed))
            if(button == True):
                print('Square Wave')
                i = 1
                button = False
                reset_timer()
                continue

 
