# -*- coding: utf-8 -*-

'''@file                mainpage.py
   @brief               Brief doc for mainpage.py
   @details             Detailed doc for mainpage.py

   @mainpage

   @section sec_intro   Introduction
                        Some information about lab 1: The user can push a button to light an LED that switches between three wave forms square, sawtooth, and sin(x) until the program is exited
                        Here is the link of our source code: https://bitbucket.org/IshanJandaur/me305-lab/src/master/
                        \image html State_Machine_Lab1.jpg
                        
                        Youtube video of working code:

                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/BiUkPta3oZA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly
                        
                        Some information about Lab 2: Our program is an interactive program that gives a user the ability to interact with an encoder. By pressing certain characters the user is able
                        to print the position, zero the position, obtain delta, and collect data of the encoder for 30 seconds or less.
                        
   @image html Lab2_Task_Encoder_State_Machine.jpg
   @image html Lab2_Task_User_State_Machine.jpg
   @image html Lab2_Main_Task_Diagram.jpg
                        
                    
   @author              Ishan Jandaur

   @copyright           License Info

   @date                October 8, 2021
'''

