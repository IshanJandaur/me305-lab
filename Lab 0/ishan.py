def fib(idx):#Fibonacci calculation as its own function that is called in main
    list = [0, 1] #list with first two fibonacci numbers
    i = 2 #index for next element in list
    while i <= idx: #while loop to iterate until last element in list is index
        x = list[i - 1] + list[i - 2] #calculate next element in list 
        list.append(x) #add next element to list 
        i += 1 #add 1 to index and go through loop again
    return list[idx] # return last number in list since it should be the same as index you want
        

if __name__ == '__main__':
    while True: #allows loop to run until it breaks due to user input
        idx = input('Choose an index: ') # prompts user to select index from Fibonacci sequence
        if idx.isnumeric() == True: # checks if user string input gives a numerical value
            idx = int(idx) # convert index to an integer
            if idx >= 0: # check if index integer is greater than zero
                print ('Fibonacci number at '
                       'index {:} is {:}.'.format(idx,fib(idx))) #run fib(idx) and print result to user
                reply = input('Enter q to quit or press enter to continue:') # gives user option to continue program or exit
                if reply == 'q': #option to quit program
                    break 
                else: #option to continue program
                    continue
        else: 
            if '-' in idx and int(idx) < 0: #checks if user inputs negative integer
                print('The index must be nonnegative!')
            else: #checks if user inputs an enter, non numerical character, or a mix of numbers and non numerical characters
                print('The index must be an integer!')