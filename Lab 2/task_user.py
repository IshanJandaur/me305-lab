''' @file       task_user.py
    @brief      User interface task for cooperative multitasking example.
    @details    Implements a finite state machine and gives ability for user to interact with encoder.
    If the user hits the character z, the position of the encoder is 0
    If the user hits the character p, the position of the encoder is printed to the screen
    If the user hits the character d, the change in position for the encoder is printed
    If the user hits character g, the encoder collects data for 30 seconds and prints a list of this data to the screen. 
    However, if character s is hit before the 30 seconds is complete, the data collection ends prematurely and the list is printed to the screen.
    @author Ishan Jandaur
    @author Soren Barclay
    @date October 21, 2021
    @copyright
'''

import utime, pyb
from micropython import const
#import csv

## State 0 of the user interface task
S0 = const(0)
## State 1 of the user interface task
S1 = const(1)
## State 2 of the user interface task
S2 = const(2)

class Task_User():
    ''' @brief      User interface task for encoder use
        @details    Creates a class that takes in period, position, delta, and zero.
    '''
    
    def __init__(self, period, position, delta, zero):
        ''' @brief              Constructs an user task.
            @details            The user task is implemented creates attributes of an object of the class.
            @param period       The larget number that can be stored on the timer. 
            @param position     Position of the encoder shared amongst files through shares file
            @param delta        Delta of the encoder shared amongst files through shares file
            @param zero         A flag that determines whether user prompts encoder to make current position the value of 0
            @param dbg          A flag that determines if a state transition occurs
            @param list1        List where data is collected and stored before being printed to the screen.
        '''
        self.period = period
        self.position = position
        self.delta = delta
        self.zero = zero
        self.dbg = False
        self.list1 = []
        
        ## A serial port to use for user I/O
        self.ser = pyb.USB_VCP()
        ## The state to run on the next iteration of the finite state machine
        self.state = S0
        
        ## The utime.ticks_us() value associated with the next run of the FSM
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        self.counter = 0
        self.start_time = 0
        self.char_in = ' '
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
            @details Runs entire user interface that takes in user input to transition from state to state.
        ''' 
        current_time = utime.ticks_us()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if self.state == S0:
                print('''
                Welcome to encoder controller.
                press z to zero out the encoder
                press p to print out the position of encoder 1
                press d to print out the delta for encoder 1
                press g to collect data for 30 seconds and return a csv
                press s to end data collection prematurely
                      ''')
                self.transition(S1)
                
            elif self.state == S1:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode().upper()
                    if(self.char_in == 'Z'):
                        print('position has been zeroed')
                        self.zero.write(1)
                    elif(self.char_in == 'P'):
                        print('reading position')
                        print(self.position.read())
                    elif(self.char_in == 'D'):
                        print('reading delta')
                        print(self.delta.read())
                    elif(self.char_in == 'G'):
                        self.state = 2
                    else:
                        print('Command \'{:}\' is invalid.'.format(self.char_in))
                            
            elif self.state == S2:
                if self.ser.any():
                    self.char_in = self.ser.read(1).decode().upper()
                if self.counter == 0: 
                    print('Collecting encoder data')
                    self.start_time = utime.ticks_ms()
                    self.counter = 1
                                
                if utime.ticks_ms() - self.start_time <= 30000:
                    self.list1.append(self.position.read())
                    if self.char_in == 'S':
                            print('Data collection ended prematurely')
                            self.start_time = -30000 #regardless of the current utim.ticks time it will be above 30,000 and the list will be printed
                else:    
                    print(self.list1)
                    self.list1 = []
                    self.counter = 0
                    self.state = 1
                    
                        
            else:
                raise ValueError('Invalid State')
            
            self.next_time = utime.ticks_add(self.next_time, self.period)
    
    def transition(self, new_state):
        ''' @brief      Transitions the FSM to a new state
            @details    Additionally messages can be printed during transition to help with debugging process.
            @param      new_state The state to transition to.
        '''
        if (self.dbg):
            print('{:}: S{:}->S{:}'.format(self.name,self.state,new_state))
        self.state = new_state