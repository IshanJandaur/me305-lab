''' @file       main_lab02.py
    @brief      Main file that instantiates objects and runs them in a loop
    @details    Three shares abojects are created for the task encoder and task
                user to communicate with each other. These tasks use the shares 
                as input parameters. The program runs until the user presses 
                Ctrl+C
'''

import pyb
import shares
import task_user
import task_encoder

def main():
    ''' @brief The main program
        @details The function instantiates three share variables, 
        a user class, and an encoder class then runs the two tasks until an 
        interrupt is recieved
    '''

    position = shares.Share(0)
    delta = shares.Share(0)
    zero = shares.Share(0)

    ## An user interface task object
    user = task_user.Task_User(10000, position, delta, zero)
    ## A user encoder object
    encoder = task_encoder.Task_Encoder(10000, position, delta, zero)
    
    ## A list of tasks to run
    task_list = [user, encoder]
    
    while(True):
        try:
            for task in task_list:
                task.run()
        except KeyboardInterrupt:
            break
    
    print('Program Terminating')
    
if __name__ == '__main__':
    main()

