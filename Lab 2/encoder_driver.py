import pyb

''' 
@file encoder.py
@brief A driver for reading from Quadrature Encoders
@details This file serves to measure movement of a motor with an optical encoder. The encoder is read at high frequencies
with the use of timers. The file serves as a way to interact with the hardware to obtain data from the encoder. 
@author Ishan Jandaur
@author Soren Barclay
@date January 1, 1970
@copyright
'''
class Encoder:
    ''' 
    @brief Interface with quadrature encoders
    @details
    '''    
    def __init__(self, timNum, pinA, pinB):
        
        self.position = 0
        self.pinA = pyb.Pin(pyb.Pin.cpu.B6)
        self.pinB = pyb.Pin(pyb.Pin.cpu.B7)
        self.tim4 = pyb.Timer(timNum, prescaler = 0, period = 65535)
        self.t4ch1 = self.tim4.channel(1, pyb.Timer.ENC_AB, pin=self.pinA)
        self.t4ch2 = self.tim4.channel(2, pyb.Timer.ENC_AB, pin=self.pinB)
        self.old = 0

        print('Creating encoder object')

    def update(self):
        '''
        @brief updates position every using timer 
        @details Reads the timer rapidly and repeatdly and computes the change in position
        to make sure the timer count does not overflow. This is accomplished by adding or subtracting delta to the position
        '''
        
        self.new = self.tim4.counter()
        self.delta = self.new - self.old
        self.old = self.tim4.counter()
        
        if(self.delta < -65535/2):
            self.delta += 65536
            
        elif(self.delta > 65535/2):
            self.delta -= 65534

        self.position += self.delta 
            
            
        #print('Reading encoder count and updating position and delta values')

    def get_position(self):
        ''' 
        @brief Returns encoder position
        @details Obtains data from the update_method on what the current position of the encoder is
        @return The position of the encoder shaft
        '''
        
        return self.position
    
    def set_position(self, position):
        '''
        @brief Sets encoder position
        @details Obtains position from the update function and sets it to a position that is input into the function parameters. 
        @param position The new position of the encoder shaft
        '''

        self.position = position
        self.old = self.tim4.counter()
        

    def get_delta(self):
        ''' 
        @brief Returns encoder delta
        @details Obtains delta from the update function 
        @return The change in position of the encoder shaft
        between the two most recent updates
        '''
        return self.delta      
        