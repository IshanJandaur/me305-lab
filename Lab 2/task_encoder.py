''' @file       task_encoder.py
    @brief      Interacts with the encoder_driver to record information about the encoder
    @details    This task first initializes the input parameters in the constructor,
                then runs a loop that updates the encoder and records the position, 
                delta, and zeros the encoder if prompted
    
'''

import utime
import encoder_driver 

class Task_Encoder():
    ''' @brief      Encoder task to interact with the encoder driver
        @details    Implements a finite state machine
    '''
    
    def __init__(self, period, position, delta, zero):
        ''' @brief              Constructs an encoder task
            @details            Task encoder object is implemented as a finite state machine 
            @param period       Shares of the period, in microseconds, 
                                between runs of the task.
            @param position     Shares of the encoder position
            @param delta        Shares of the encoder delta
            @param zero         Shares of flag to zero the encoder
        '''
        ## The name of the task
        self.period = period
        self.position = position
        self.delta = delta
        self.zero = zero
        self.state = 0
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        self.enc = encoder_driver.Encoder(4, 'pinB6', 'pinB7')
        
    def run(self):
        ''' @brief Runs one iteration of the FSM
            @details begins by checking the zero flag logic. Then, if the task 
            is running within the time limit and time has past. 
            Then the encoder is updated, and position and delta are recorded 
        '''
        current_time = utime.ticks_us()
        
        if self.zero.read() == 1:
            self.position.write(self.enc.set_position(0))
            self.zero.write(0)
                
        if (utime.ticks_diff(current_time, self.next_time) >= 0):                 
            self.enc.update()
            self.position.write(self.enc.get_position())
            self.delta.write(self.enc.get_delta())     

                
            self.next_time = utime.ticks_add(self.next_time, self.period)

    

        